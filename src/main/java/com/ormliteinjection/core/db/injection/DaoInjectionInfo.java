package com.ormliteinjection.core.db.injection;

import com.ormliteinjection.core.db.helper.XDBHelper;

/**
 * Created by rramirezb on 22/12/2014.
 */
public @interface DaoInjectionInfo {
    String daoPackage();
    String ormHelperName() default "OrmHelper";
    String daosRefName() default "Daos";
    XDBHelper.Platform platform() default XDBHelper.Platform.JAVA;
}
