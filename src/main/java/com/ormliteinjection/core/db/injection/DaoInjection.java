package com.ormliteinjection.core.db.injection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * To use, need first implements {@link com.ormliteinjection.core.db.helper.JavaDatabaseInfo}
 * or {@link com.ormliteinjection.core.db.helper.AndroidDatabaseInfo}.
 * You must add {@link DaoInjectionInfo} annotation in
 * the implemented class.
 * Created by rramirezb on 18/12/2014.
 *
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface DaoInjection {
    String idField() default "";
    String idMethod() default "";
    boolean generatedId() default true;
}
