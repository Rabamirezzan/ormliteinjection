package com.ormliteinjection.core.db.dao;

public @interface ContainerReference {
boolean deleteOnContainerDelete() default true;
}
