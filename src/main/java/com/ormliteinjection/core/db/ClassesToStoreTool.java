package com.ormliteinjection.core.db;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

public class ClassesToStoreTool {
	public static Set<Class<?>> getClasses(Class<? extends ClassesToStore> cls) {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		if (cls != null) {
			Field[] fs = cls.getDeclaredFields();

			for (Field field : fs) {
				boolean isValidField = Modifier.isPublic(field.getModifiers())
						&& Modifier.isFinal(field.getModifiers());
				if (isValidField && field.getName().startsWith("model"))
					try {
						Object value = field.get(null);
						if (value instanceof Class) {
							classes.add((Class<?>) value);
						}
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		return classes;
	}
}
