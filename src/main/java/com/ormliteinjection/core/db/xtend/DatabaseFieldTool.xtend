package com.opesystems.tools.ormmodel.db.xtend

import com.j256.ormlite.field.DatabaseField
import java.lang.reflect.Field
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference

class DatabaseFieldTool {

	static val String COLUMN_NAME = "columnName"
	static val String FOREIGN = "foreign"
	@DatabaseField
	int x;

	static def getDatabaseFieldAnnotation(MutableFieldDeclaration field) {
		val ann = field.annotations.findFirst[a|a.annotationTypeDeclaration.qualifiedName == DatabaseField.name]
		ann
	}

	static def getDatabaseFieldAnnotation(Field field) {
		val ann = field.getAnnotation(DatabaseField)
		ann as DatabaseField
	}

	static def containsDatabaseFieldAnnotation(MutableFieldDeclaration field) {
		val exists = field.databaseFieldAnnotation != null
		exists
	}

	static def getFieldId(Class<?> annotatedClass) {
		val field = annotatedClass.declaredFields.findFirst[f|f.id]
		field

	}

	static def getIdValue(Object model) {
		val field = getFieldId(model.class);
		if (field != null)
			field.accessible = true
		field?.get(model);
	}

	static def getIdType(MutableClassDeclaration annotatedClass) {
		var TypeReference type;
		val field = annotatedClass.declaredFields.findFirst[f|f.id]
		type = field?.type

	}

	static def isId(MutableFieldDeclaration field) {
		val ann = field.databaseFieldAnnotation;
		ann?.getBooleanValue("id") || ann?.getBooleanValue("generatedId")
	}

	static def isId(Field field) {
		val ann = field.databaseFieldAnnotation;

		ann?.id || ann?.generatedId
	}

	static def isForeign(Field field) {
		val ann = field.databaseFieldAnnotation;

		ann?.foreign
	}

	static def isForeign(MutableFieldDeclaration field) {
		val ann = field.databaseFieldAnnotation;

		ann?.getBooleanValue("foreign")
	}

	static def getName(MutableFieldDeclaration field) {

		var String result = null;
		val a = field.annotations.findFirst[a|a.annotationTypeDeclaration.qualifiedName == DatabaseField.name]

		println(a)
		val columnName = a.getStringValue(COLUMN_NAME);
		val foreign = a.getBooleanValue(FOREIGN);
		println(columnName)

		if (foreign && columnName.trim.nullOrEmpty) {
			result = field.simpleName + "_id"
		} else {
			result = if(!columnName.trim.nullOrEmpty) columnName else field.simpleName;
		}
		result
	}

}
