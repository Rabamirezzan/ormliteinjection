package com.opesystems.tools.ormmodel.db.xtend

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.Target
import org.eclipse.xtend.lib.macro.Active

@Target(ElementType.TYPE)
@Retention(RUNTIME)
@Active(PersistentModelProcessor)
annotation XDaoFun {
	String daoPackage
	boolean internalCall = false
}