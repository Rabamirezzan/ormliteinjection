package com.ormliteinjection.core.db.xtend;

import com.j256.ormlite.stmt.*;
import com.ormliteinjection.core.db.exception.NoNameFieldCacheException;
import com.ormliteinjection.core.db.exception.SQLRuntimeException;
import com.ormliteinjection.core.db.tool.StringJoin;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@SuppressWarnings("all")
public class XStatement<T, ID, S extends XStatement> {

	private StatementBuilder<T, ID> builder;
	protected Where<T, ID> where;
	private AtomicReference<Class<?>> classFieldCache = new AtomicReference<Class<?>>();
	private AtomicReference<String> nameFieldCache = new AtomicReference<String>();
	private AtomicReference<List<String>> columnsCache = new AtomicReference<List<String>>();

	private AtomicBoolean foreignFieldCache = new AtomicBoolean();
	private boolean selectCall = false;
	private String groupTag="";

	public XStatement(StatementBuilder<T, ID> builder) {
		this.builder = builder;

	}

	public S where(){
		if(isSelectCall()){
			setSelectParams();
		}
		return (S) this;
	}

	protected void setSelectParams(){

		//String raw = getConcatedColumnsNames("ASC");
		if(hasColumns(columnsCache)) {
			qb().selectColumns(columnsCache.get());
			resetColumnsCache();
		}
	}

	protected Where<T, ID> getWhere() {
		if (where == null) {
			this.where = builder.where();
		}
		return where;
	}

	// /**
	// * Shortcut for a new XWhere instance.
	// *
	// * @return
	// */
	// public W w() {
	// return (W) XDao.getInstance(this.getClass(), builder);
	// }

	protected String getNameField() throws NoNameFieldCacheException {
		String name = nameFieldCache.get();
		if (name == null) {
			throw new NoNameFieldCacheException("No nameField selected");
		} else {
			nameFieldCache.set(null);
		}
		return name;
	}

	protected boolean isForeign() {
		boolean result = false;

		result = classFieldCache.get() != null;
		return result;

	}

	protected Object getValueIfForeign(Object value) {
		Object result = value;
		if (isForeign() && classFieldCache.get().isInstance(value)) {
			result = DatabaseFieldTool.getIdValue(value);
			setForeignClass(null);
		}
		return result;

	}

	public S setForeignClass(Class<?> foreign) {
		classFieldCache.set(foreign);
		return (S) this;
	}

	public S setNameField(String name) {
		nameFieldCache.set(name);
		return (S) this;
	}

	public static <T, ID, W extends XStatement> Where<T, ID>[] get(
			XStatement<T, ID, W>... list) {
		List<Where<T, ID>> _others = new ArrayList<Where<T, ID>>();

		for (XStatement<T, ID, W> xStatement : list) {
			_others.add(xStatement.getWhere());
		}
		return (Where<T, ID>[]) _others.toArray(new Where[] {});
	}

	/**
	 * Get QueryBuilder.
	 * 
	 * @return
	 */
	public QueryBuilder<T, ID> qb() {
		QueryBuilder<T, ID> result = null;
		if (builder instanceof QueryBuilder) {
			result = ((QueryBuilder<T, ID>) builder);

		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					QueryBuilder.class.getName()));
		}
		return result;
	}

	/**
	 * Get UpdateBuilder.
	 * 
	 * @return
	 */
	public UpdateBuilder<T, ID> ub() {
		UpdateBuilder<T, ID> result = null;
		if (builder instanceof UpdateBuilder) {
			result = ((UpdateBuilder<T, ID>) builder);

		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					UpdateBuilder.class.getName()));
		}
		return result;
	}

	/**
	 * Get DeleteBuilder.
	 * 
	 * @return
	 */
	public DeleteBuilder<T, ID> db() {
		DeleteBuilder<T, ID> result = null;
		if (builder instanceof DeleteBuilder) {
			result = ((DeleteBuilder<T, ID>) builder);

		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					DeleteBuilder.class.getName()));
		}
		return result;
	}

	/**
	 * See {@link Where#and()}
	 * 
	 * @return
	 */
	public S and() {
		getWhere().and();
		return (S) this;
	}

	/**
	 * See {@link Where#and(Where, Where, Where...)}
	 * 
	 * @param first
	 * @param second
	 * @param others
	 * @return
	 */
	public S and(S first, S second, S... others) {

		getWhere().and(first.getWhere(), second.getWhere(), get(others));
		return (S) this;
	}

	/**
	 * See {@link Where#between(String, Object, Object)}
	 * 
	 * @param low
	 * @param high
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S between(Object low, Object high) throws SQLRuntimeException,
			NoNameFieldCacheException {
		try {
			getWhere().between(getNameField(), low, high);
		} catch (SQLException e) {

			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#countOf()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public long countOf() throws SQLRuntimeException, NoNameFieldCacheException {
		long result = 0;
		try {
			result = getWhere().countOf();
		} catch (SQLException e) {

			throw new SQLRuntimeException(e);
		}
		return result;
	}

	/**
	 * See {@link Where#eq(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S eq(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		Object _value = getValueIfForeign(value);
		try {
			getWhere().eq(getNameField(), _value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#ge(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S ge(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().ge(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#gt(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S gt(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().gt(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#idEq(Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S idEq(ID value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().idEq(value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#in(String, Object...)}
	 * 
	 * @param obs
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public <O> S in(O... obs) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().in(getNameField(), obs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#in(String, Iterable)}
	 * 
	 * @param obs
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public <O> S in(Iterable<O> obs) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().in(getNameField(), obs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#in(String, QueryBuilder)}
	 * 
	 * @param query
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S in(QueryBuilder<?, ?> query) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().in(getNameField(), query);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#in(String, QueryBuilder)}
	 *
	 * @param query
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S in(XStatement<?, ?, ?> statement) throws SQLRuntimeException,
			NoNameFieldCacheException {
		return (S) in(statement.qb());
	}

	/**
	 * See {@link Where#isNotNull(String)}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S isNotNull() throws SQLRuntimeException, NoNameFieldCacheException {

		try {
			getWhere().isNotNull(getNameField());
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#isNull(String)}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S isNull() throws SQLRuntimeException, NoNameFieldCacheException {

		try {
			getWhere().isNull(getNameField());
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#le(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S le(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().le(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#like(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S like(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().like(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#lt(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S lt(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().lt(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#ne(String, Object)}
	 * 
	 * @param value
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S ne(Object value) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().ne(getNameField(), value);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#not()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S not() throws SQLRuntimeException, NoNameFieldCacheException {

		getWhere().not();

		return (S) this;
	}

	/**
	 * See {@link Where#not(Where)}
	 * 
	 * @param where
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S not(XStatement<T, ID, S> where) throws SQLRuntimeException,
			NoNameFieldCacheException {

		this.getWhere().not(where.getWhere());

		return (S) this;
	}

	/**
	 * See {@link Where#notIn(String, Iterable)}
	 * 
	 * @param objects
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public <O> S notIn(Iterable<O> objects)
			throws SQLRuntimeException, NoNameFieldCacheException {

		try {
			getWhere().notIn(getNameField(), objects);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#notIn(String, Object...)}
	 * 
	 * @param objects
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public <O> S notIn(O... objects) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().notIn(getNameField(), objects);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#notIn(String, QueryBuilder)}
	 * 
	 * @param query
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S notIn(QueryBuilder<?, ?> query) throws SQLRuntimeException,
			NoNameFieldCacheException {

		try {
			getWhere().notIn(getNameField(), query);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return (S) this;
	}

	/**
	 * See {@link Where#notIn(String, QueryBuilder)}
	 *
	 * @param query
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S notIn(XStatement<?, ? , ?> statement) throws SQLRuntimeException,
			NoNameFieldCacheException {


		return (S) notIn(statement.qb());
	}

	/**
	 * See {@link Where#or()}
	 */
	public S or() throws SQLRuntimeException, NoNameFieldCacheException {

		getWhere().or();

		return (S) this;
	}

	/**
	 * See {@link Where#or(Where, Where, Where...)}
	 * 
	 * @param first
	 * @param second
	 * @param others
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public S or(S first, S second, S... others) throws SQLRuntimeException,
			NoNameFieldCacheException {

		getWhere().or(first.getWhere(), second.getWhere(), get(others));

		return (S) this;
	}

	/**
	 * See {@link QueryBuilder#query()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public List<T> query() throws SQLRuntimeException,
			NoNameFieldCacheException {

		List<T> result;
		try {
			result = qb().query();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return result;
	}

	/**
	 * See {@link QueryBuilder#queryForFirst()}
	 * 
	 * @param defaultValue
	 * @return
	 * @throws SQLRuntimeException
	 * @throws NoNameFieldCacheException
	 */
	public T queryForFirst(T defaultValue) throws SQLRuntimeException,
			NoNameFieldCacheException {
		T result = null;
		try {
			result = qb().queryForFirst();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		if (result == null) {
			result = defaultValue;
		}
		return result;
	}

	/**
	 * Shortcut to {@link UpdateBuilder#update()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 */
	public int update() throws SQLRuntimeException {
		int result = 0;
		if (builder instanceof UpdateBuilder) {
			try {
				result = ((UpdateBuilder<T, ID>) builder).update();
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					UpdateBuilder.class.getName()));
		}
		return result;
	}

	/**
	 * Shortcut to {@link DeleteBuilder#delete()}
	 * 
	 * @return
	 * @throws SQLRuntimeException
	 */
	public int delete() throws SQLRuntimeException {
		int result = 0;
		if (builder instanceof DeleteBuilder) {
			try {
				result = ((DeleteBuilder<T, ID>) builder).delete();
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			}
		} else {
			throw new SQLRuntimeException(String.format("No %s statement",
					DeleteBuilder.class.getName()));
		}
		return result;
	}

	public String getPrepareStatementString() throws SQLRuntimeException {
		String result = "";
		try {
			result = builder.prepareStatementString();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		return result;
	}

	protected static boolean hasColumns(AtomicReference<List<String>> columns) {
		return columns.get() != null;
	}

	protected boolean hasCacheColumns() {
		return hasColumns(columnsCache);
	}

	protected void addColumnToCache(String column) {
		if (hasColumns(columnsCache)) {
			columnsCache.get().add(String.format("%1$s%2$s%1$s", groupTag, column));
		}
	}

	public S orderBy() {
		initColumnsCache("`");
		return (S) this;
	}

	protected String getConcatedColumnsNames(String order) {
		List<String> columns = columnsCache.get();

		String result = StringJoin.join(columns, ",");
		return result.concat(" ").concat(order);
	}

	protected void initColumnsCache(String groupTag){
		columnsCache.set(new ArrayList<String>());
		this.groupTag = groupTag;
	}
	protected void resetColumnsCache() {
		columnsCache.set(null);
	}

	public S desc() {
		String raw = getConcatedColumnsNames("DESC");
		qb().orderByRaw(raw);
		resetColumnsCache();
		return (S) this;
	}

	public S asc() {
		String raw = getConcatedColumnsNames("ASC");
		qb().orderByRaw(raw);
		resetColumnsCache();
		return (S) this;
	}



	public void setSelectCall(boolean selectCall) {
		this.selectCall = selectCall;
		initColumnsCache("");
	}

	public boolean isSelectCall() {
		return selectCall;
	}
}
