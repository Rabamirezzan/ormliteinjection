package com.ormliteinjection.core.db.xtend;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.StatementBuilder;
import com.ormliteinjection.core.db.DBInstance;
import com.ormliteinjection.core.db.exception.SQLRuntimeException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class XDao<T, ID, S extends XStatement<?, ?, ?>> {

	private final DBInstance db;
	private final Class<T> cls;
	private final Class<S> statementClass;

	public XDao(DBInstance db, Class<T> modelClass, Class<S> statementClass) {
		this.db = db;
		this.cls = modelClass;
		this.statementClass = statementClass;
	}

	public DBInstance getDb() {
		return db;
	}

	public S selectAllWhere() {
		Dao<T, ID> dao = db.getDao(cls);
		S statement = getInstance(statementClass, dao.queryBuilder());
		return statement;
	}

	public S select(){
		Dao<T, ID> dao = db.getDao(cls);
		S statement = getInstance(statementClass, dao.queryBuilder());
		statement.setSelectCall(true);
		return statement;
	}

	public S updateWhere() {
		Dao<T, ID> dao = db.getDao(cls);
		S statement = getInstance(statementClass, dao.updateBuilder());
		return statement;
	}

	public S deleteWhere() {
		Dao<T, ID> dao = db.getDao(cls);
		S statement = getInstance(statementClass, dao.deleteBuilder());
		return statement;
	}

	public List<T> query() throws SQLRuntimeException {
		Dao<T, ID> dao = db.getDao(cls);
		List<T> result = new ArrayList<T>();
		try {
			result  = dao.queryBuilder().query();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
		
		return result;
	}

	public static <T> T getInstance(Class<T> clazz, Object... params) {
		T result = null;
		try {
			Constructor<T> constructor = clazz
					.getDeclaredConstructor(StatementBuilder.class);
			result = constructor.newInstance(params);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * See {@link Dao#createOrUpdate(Object)}
	 * 
	 * @param model
	 */
	public void save(T model) throws SQLRuntimeException {

		try {
			db.getDao(cls).createOrUpdate(model);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}

	}

	/**
	 * See {@link Dao#delete(Object)}
	 * 
	 * @param model
	 */
	public void delete(T model) throws SQLRuntimeException {

		try {
			db.getDao(cls).delete(model);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}

	}

	/**
	 * See {@link Dao#delete(Collection)}
	 * 
	 * @param datas
	 */
	public void delete(Collection<T> datas) throws SQLRuntimeException {

		try {
			db.getDao(cls).delete(datas);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}

	}

	/**
	 * See {@link Dao#create(Object)}
	 *
	 * @param model
	 */
	public void create(T model) throws SQLRuntimeException {
		try {
			db.getDao(cls).create(model);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * See {@link Dao#update(Object)}
	 * 
	 * @param model
	 */
	public void update(T model) throws SQLRuntimeException {
		try {
			db.getDao(cls).update(model);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * See {@link Dao#refresh(Object)}
	 * 
	 * @param model
	 */
	public void refresh(T model) {
		try {
			db.getDao(cls).refresh(model);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * Future implementation.
	 */
	public void raw(){

	}
}
