package com.ormliteinjection.core.db.xtend;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.j256.ormlite.field.DatabaseField;
//import com.sun.org.apache.xpath.internal.operations.Variable;

import javax.lang.model.element.VariableElement;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * Created by rramirezb on 18/12/2014.
 */
public class DatabaseFieldTool {
    public static DatabaseField getDatabaseFieldAnnotation(Field field) {
        DatabaseField ann = field.getAnnotation(DatabaseField.class);
        return ann;
    }

    public static DatabaseField getDatabaseFieldAnnotation(VariableElement field){
        DatabaseField ann = field.getAnnotation(DatabaseField.class);
        return ann;
    }

    public static boolean containsDatabaseFieldAnnotation(VariableElement field){
        return getDatabaseFieldAnnotation(field)!=null;
    }

    public static boolean isId(Field field) {
        return isId(getDatabaseFieldAnnotation(field));
    }

    public static boolean isId(DatabaseField ann) {


        boolean result = false;
        if(ann!=null){
            result = ann.id() || ann.generatedId();
        }
        return result;
    }
    public static Field getFieldId(Class<?> annotatedClass) {
        Field[] fields = annotatedClass.getDeclaredFields();
        Field field = Iterables.find(Arrays.asList(fields), new Predicate<Field>() {
            @Override
            public boolean apply(Field input) {
                return isId(input);
            }
        });

        return field;

    }
    public static Object getIdValue(Object model) {
        Field field = getFieldId(model.getClass());
        Object result= null;
        if (field != null) {
            field.setAccessible(true);
            try {
                result = field.get(model);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static String getName(VariableElement field) {

        String result = null;
        DatabaseField a = field.getAnnotation(DatabaseField.class);

        String columnName = a.columnName();
        boolean foreign = isForeign(a);
        boolean hasName = columnName!=null && !columnName.trim().equals("");
        if (foreign && !hasName) {
            result = field.getSimpleName() + "_id";
        } else {
            if(hasName) {
                result = columnName.trim();
            }else{
                result = field.getSimpleName().toString();
            }

        }
        return result;
    }

    public static boolean isForeign(VariableElement field){
        DatabaseField a = field.getAnnotation(DatabaseField.class);
        return isForeign(a);
    }
    public static boolean isForeign(DatabaseField a){
        return a.foreign();
    }
}
