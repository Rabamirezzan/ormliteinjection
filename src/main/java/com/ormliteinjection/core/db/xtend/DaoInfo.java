package com.ormliteinjection.core.db.xtend;

public interface DaoInfo {
	public String globalDao();
	public String dbInfo();
}
