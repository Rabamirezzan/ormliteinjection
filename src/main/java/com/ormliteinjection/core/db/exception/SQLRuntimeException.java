package com.ormliteinjection.core.db.exception;

public class SQLRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SQLRuntimeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SQLRuntimeException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

	public SQLRuntimeException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public SQLRuntimeException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	
}
