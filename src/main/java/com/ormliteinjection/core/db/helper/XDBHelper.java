package com.ormliteinjection.core.db.helper;

import android.content.Context;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.ormliteinjection.core.db.ClassesToStore;
import com.ormliteinjection.core.db.DBHelper;
import com.ormliteinjection.core.db.DBInstance;
import com.ormliteinjection.core.db.XOrmLiteSqliteOpenHelper;

public abstract class XDBHelper extends DBHelper {

	public static enum Platform {
		JAVA, ANDROID, BOTH
	};

	private final DatabaseInfo info;

	private final Context context;
	private final AndroidDatabaseInfo androidInfo;
	
	private final Platform platform;

	public XDBHelper(JavaDatabaseInfo info) {
		super();
		this.info = info;
		this.context = null;
		this.androidInfo = null;
		platform = Platform.JAVA;
	}
	
	public XDBHelper(Context context, AndroidDatabaseInfo info) {
		super();
		this.info = null;
		this.context = context;
		this.androidInfo = info;
		platform = Platform.ANDROID;
	}

	@Override
	public DBInstance newInstance(
			java.lang.Class<? extends ClassesToStore> dbInfoClass) {
		DBInstance instance = null;
		if(Platform.JAVA.equals(platform)) {
			instance = _newJavaInstance(dbInfoClass);
		}else if(Platform.ANDROID.equals(platform)){
			instance = _newAndroidInstance(dbInfoClass);
		}
		return instance;
	};

	
	
	protected DBInstance _newJavaInstance(
			java.lang.Class<? extends ClassesToStore> dbInfoClass) {
		final String DATABASE_URL = ((JavaDatabaseInfo)info).getUrl();
		DBInstance instance = null;
		try {
			final ConnectionSource connectionSource = new JdbcConnectionSource(
					DATABASE_URL);
			instance = new DBInstance(connectionSource, dbInfoClass, new JavaTransaction(connectionSource));

			instance.createAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instance;

	};
	
	protected DBInstance _newAndroidInstance(Class<? extends ClassesToStore> classesToStore) {

		return new XOrmLiteSqliteOpenHelper(context, androidInfo.getName(), null,
				androidInfo.getVersion(), classesToStore).getDBInstance();
	}

}
