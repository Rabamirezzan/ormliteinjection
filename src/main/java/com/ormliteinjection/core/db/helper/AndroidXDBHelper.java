package com.ormliteinjection.core.db.helper;

import android.content.Context;

import com.ormliteinjection.core.db.ClassesToStore;
import com.ormliteinjection.core.db.DBHelper;
import com.ormliteinjection.core.db.DBInstance;
import com.ormliteinjection.core.db.XOrmLiteSqliteOpenHelper;

public abstract class AndroidXDBHelper extends DBHelper {

	private Context context;
	private AndroidDatabaseInfo info;

	public AndroidXDBHelper(Context context, AndroidDatabaseInfo info) {
		this.context = context;
		this.info = info;
	}

	@Override
	public DBInstance newInstance(Class<? extends ClassesToStore> classesToStore) {

		return new XOrmLiteSqliteOpenHelper(context, info.getName(), null,
				info.getVersion(), classesToStore).getDBInstance();
	}

	// public DBInstance getInstance() {
	//
	// return getInstance("ABC", DBClasses.class);
	//
	// }

}
