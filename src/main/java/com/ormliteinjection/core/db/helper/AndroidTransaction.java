package com.ormliteinjection.core.db.helper;

import com.ormliteinjection.core.db.Transaction;
import com.ormliteinjection.core.db.XOrmLiteSqliteOpenHelper;

import java.sql.SQLException;

/**
 * Created by rramirezb on 02/01/2015.
 */
public class AndroidTransaction implements Transaction {
    private XOrmLiteSqliteOpenHelper helper;

    public AndroidTransaction(XOrmLiteSqliteOpenHelper helper) {
        this.helper = helper;
    }

    @Override
    public void beginTransaction() throws SQLException {
        helper.getReadableDatabase().beginTransaction();
    }

    @Override
    public void endTransaction() throws SQLException{
        helper.getReadableDatabase().setTransactionSuccessful();
        helper.getReadableDatabase().endTransaction();
    }

    @Override
    public void cancelTransaction() throws SQLException{
        helper.getReadableDatabase().endTransaction();
    }
}
