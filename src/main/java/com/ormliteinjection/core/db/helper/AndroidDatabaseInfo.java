package com.ormliteinjection.core.db.helper;

public interface AndroidDatabaseInfo extends DatabaseInfo{
	public String getName();

	public int getVersion();
}
