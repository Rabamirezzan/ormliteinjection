package com.ormliteinjection.core.db.helper;

import com.j256.ormlite.support.ConnectionSource;
import com.ormliteinjection.core.db.Transaction;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.UUID;

/**
 * Created by rramirezb on 02/01/2015.
 */
public class JavaTransaction implements Transaction {
    public Savepoint savePoint;
    ConnectionSource connectionSource;

    public JavaTransaction(ConnectionSource connectionSource) {
        this.connectionSource = connectionSource;
    }

    public String getSavePointTag(){
        return UUID.randomUUID().toString();
    }
    @Override
    public void beginTransaction() throws SQLException {

        connectionSource.getReadWriteConnection().setAutoCommit(false);
        savePoint = connectionSource.getReadWriteConnection().setSavePoint(getSavePointTag());
    }

    @Override
    public void endTransaction() throws SQLException {

        connectionSource.getReadWriteConnection().commit(savePoint);

    }

    @Override
    public void cancelTransaction() throws SQLException {

        connectionSource.getReadWriteConnection().rollback(savePoint);

    }
}
