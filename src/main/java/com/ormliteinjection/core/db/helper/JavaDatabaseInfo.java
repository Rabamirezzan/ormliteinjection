package com.ormliteinjection.core.db.helper;

/**
 * Must add {@link com.ormliteinjection.core.db.injection.DaoInjectionInfo} annotation
 * in implemented class.
 * Created by rramirezb on 22/12/2014.
 */
public interface JavaDatabaseInfo extends DatabaseInfo{
    public String getUrl();
}
