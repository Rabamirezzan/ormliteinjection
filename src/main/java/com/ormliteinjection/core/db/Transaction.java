package com.ormliteinjection.core.db;

import java.sql.SQLException;

/**
 * Created by rramirezb on 02/01/2015.
 */
public interface Transaction {
    void beginTransaction() throws SQLException;

    void endTransaction()throws SQLException;

    void cancelTransaction()throws SQLException;
}
